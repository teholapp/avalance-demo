/****************************************************************************
 Copyright (c) 2017-2018 Xiamen Yaji Software Co., Ltd.
 
 http://www.cocos2d-x.org
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include "cocos2d.h"
#include <Box2D/Box2D.h>
#include "HillLayer.h"
#include "ViewPort.h"
#include "CCParallaxScrollNode.h"

USING_NS_CC;

class DebugLayer;
class ViewPort;

class HelloWorld : public cocos2d::Layer, public b2ContactListener
{
public:
    static cocos2d::Scene* createScene();

    virtual bool init();
    void menuCloseCallback(cocos2d::Ref* pSender);
	bool onTouchBegan(Touch* touch, Event* event);
	void onTouchMoved(Touch* touch, Event* event);
	void onTouchEnded(Touch* touch, Event* event);
	void onAcceleration(Acceleration *acc, Event *unused_event);
	void onKeyPressed(EventKeyboard::KeyCode keyCode, Event* event);
	void onKeyReleased(EventKeyboard::KeyCode keyCode, Event* event);
	
private:
	Sprite *generateTexture(const char *sprite, float targetHeight);
	void update(float dt);
	void definePlayer();
	void defineAvalanche();

	b2World *m_world;
	bool m_gameRunning;
	Sprite * m_player;
	float m_posX;
	float m_posY;
	bool m_gameOver;
	b2Body *m_playerBody;
	b2Body *m_avalancheBody;
	Sprite *m_avalancheSprite;
	bool m_playerExists;
	DebugLayer *m_debugLayer;
	ViewPort *m_camera;
	Layer *m_scaleLayer;
	Hill* m_hillLayer;
	Label *m_distanceLabel;
	Label *m_gameOverLabel;
	CCParallaxScrollNode *m_backGround;
	CCParallaxScrollNode *m_backGroundMid;


	CREATE_FUNC(HelloWorld);
};

#endif // __HELLOWORLD_SCENE_H__
