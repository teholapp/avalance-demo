#include <algorithm>
#include <Box2D/Box2D.h>
#include "BodyEditorLoader.h"
#include "HillLayer.h"
#include "Definitions.h"
 
USING_NS_CC;
 
Hill::Hill() {
    m_body = nullptr;
	m_avalanche = nullptr;
 
	m_visibleSize = Director::getInstance()->getVisibleSize();
    m_maxPhysicsPoints = m_visibleSize.width/STEP_LENGTH * 1.5;
        
    Sprite* snowSprite = Sprite::create("snow.png");
    snowSprite->retain();
    m_snowTexture = snowSprite->getTexture();
    m_snowTexture->setTexParameters( { GL_NEAREST, GL_NEAREST, GL_REPEAT, GL_REPEAT});
         
    GLProgram *shaderProgram_ = ShaderCache::getInstance()->getGLProgram(GLProgram::SHADER_NAME_POSITION_TEXTURE);
        setGLProgram(shaderProgram_);
    scheduleUpdate();
}
 
Hill::~Hill() {
 
}
 
bool Hill::init() {
    if(!Node::init()) {
        return false; 
    }
       
    return true;
}
 
void Hill::initWorld(b2World* world) {
	m_world = world;

	b2BodyDef bd;
	bd.position.Set(0, 0);
	bd.type = b2_staticBody;
	m_body = world->CreateBody(&bd);
}

void Hill::createObstacleBody(Sprite *sprite, ObstacleType fileType)
{
	static char *obstacleFiles[3] = {
		"rock_2_v6.json", "rock_2_v6.json", "rock_2_v6.json"
	};
	const char *filename = obstacleFiles[fileType];
	ssize_t fileSize = 0;
	char* str = reinterpret_cast<char*>(CCFileUtils::sharedFileUtils()->getFileData(filename, "rb", &fileSize));
	std::string s(str);
	delete[] str;
	BodyEditorLoader bodyLoader(s);
	b2FixtureDef obstacleFixture;
	Size size = sprite->getContentSize()*sprite->getScale();
	obstacleFixture.density = 5;
	obstacleFixture.friction = 100;
	obstacleFixture.restitution = -10;
	b2BodyDef obstacleBodyDef;
	obstacleBodyDef.type = b2_staticBody;
	obstacleBodyDef.userData = sprite;
	obstacleBodyDef.position.Set(sprite->getPosition().x / SCALE_RATIO, sprite->getPosition().y / SCALE_RATIO);
	b2Body *body;
	body = m_world->CreateBody(&obstacleBodyDef);
	bodyLoader.attachFixture(body, "obstacle", &obstacleFixture, 4.0f);
	body->SetGravityScale(0);
	m_obstacleBodies.push_back(body);
}

Size Hill::generateObstacle(const Point pos)
{
	static char *obstacleFiles[3] = {
		"rock_1.png", "rock_2.png", "lumikasa.png"
	};
	int rand = RandomHelper::random_int(0, 2);
	const char *filename = obstacleFiles[rand];
	auto sprite = Sprite::create(filename);
    float scale = (m_visibleSize.height / 8 / sprite->getContentSize().height);
	float offset = sprite->getContentSize().height * 0.3 * scale;
    sprite->setScale(scale);
	sprite->setPosition(CCPoint(pos.x, pos.y + offset));
    this->addChild(sprite, 0);
	Size size = sprite->getContentSize()*sprite->getScale();
	createObstacleBody(sprite, static_cast<ObstacleType>(rand));
	return size;
}


void Hill::generateTerrain() {
	
	m_terrainCoordinates[0].x = STEP_LENGTH;
	m_terrainCoordinates[0].y = 150.0f;

	int index = m_terrainCoordinates[0].x;
	float yOffset = m_terrainCoordinates[0].y;
	int startIndex = 0;
	int lengthInPixels = 6000;

	float downAngle = 1.0;
	int obstacleDistance = 1500;

	for (int i=0; i < MAX_TERRAIN_COORDINATES;i++) {
		float rand = RandomHelper::random_real(downAngle/2.0f, downAngle);
		if (downAngle < 1.1f) {
			rand = downAngle;
		}
		m_terrainCoordinates[i].x = index;
		yOffset -= downAngle;
		m_terrainCoordinates[i].y = yOffset;
		m_terrainCoordinates[i].type = Snow;
		index += STEP_LENGTH;
		if (index > 2000 && index % obstacleDistance == 0) {
			Size size = generateObstacle(Point(m_terrainCoordinates[i].x, m_terrainCoordinates[i].y));
		}

		if (index % 4000 == 0) {
			downAngle += 0.4;
			obstacleDistance -= 500;
			if (obstacleDistance < 500)
				obstacleDistance = 500;
		}
	}		
}

void Hill::update(float dt)
{
	float posStart = 0.0;
	float posEnd = 0.0;
	for (b2Body *body = m_world->GetBodyList(); body != NULL; body = body->GetNext()) {
		if (body->GetUserData() && body->GetType() == b2_dynamicBody)
		{
			float posX = body->GetPosition().x;
			if (posStart == 0.0 || posX < posStart)
				posStart = posX;
			
			if (posEnd == 0.0 || posX > posEnd)
				posEnd = posX;
		}
	}
	
	int start = abs(posStart * SCALE_RATIO / STEP_LENGTH);
	start = start - 20;
	if (start < 0) {
		start = 0;
	}
	int end = abs(posEnd * SCALE_RATIO / STEP_LENGTH);

	while (m_obstacleBodies.size() > 0) {
		b2Body *body = m_obstacleBodies.front();
		float x = body->GetPosition().x;
		if (x < posEnd) {
			m_world->DestroyBody(body);
			m_obstacleBodies.pop_front();
		}
		else {
			break;
		}
	}

	float distance = posEnd - posStart;
	static float last_impulse = 0;
	last_impulse += dt;
	//Speedup avalanche every 5 seconds
	if (distance > 40 && last_impulse > 5.0f) {
		last_impulse = 0.0f;
		if (m_avalanche->GetAngularVelocity() > -3.0) {
			m_avalanche->ApplyAngularImpulse(m_avalanche->GetMass()*-10, true);
		}
	}

    updatePhysics(start, end + 128);
}

void Hill::addAvalanche(b2Body *aval)
{
	m_avalanche = aval;
}

void Hill::updatePhysics(int start, int end) {
    b2EdgeShape shape;
	static int prevEnd = 0;
	if (prevEnd >= end)
		return;
	else
		prevEnd = end;

	float startXPos = m_terrainCoordinates[start].x;
	while (m_fixtures.size() > 0) {
		b2Fixture *fixture = m_fixtures.front();
		b2EdgeShape *shape = reinterpret_cast<b2EdgeShape*>(fixture->GetShape());
		float xPoint = shape->m_vertex2.x;
		if (xPoint < startXPos) {
			m_body->DestroyFixture(fixture);
			m_fixtures.pop_front();
		}
		else {
			break;
		}
	}

    for (int i= start; i < end; i++) {
		Type groundType = m_terrainCoordinates[i].type;
        b2Vec2 p1 = b2Vec2((m_terrainCoordinates[i].x)/SCALE_RATIO,m_terrainCoordinates[i].y/SCALE_RATIO);
        b2Vec2 p2 = b2Vec2((m_terrainCoordinates[i+1].x)/SCALE_RATIO,m_terrainCoordinates[i +1].y/SCALE_RATIO);
        shape.Set(p1, p2);
		b2FixtureDef fixtureDef;
		fixtureDef.density = 0;
	    fixtureDef.restitution = 0.5;
		fixtureDef.friction = 0.02;
		fixtureDef.shape = &shape;

        m_fixtures.push_back(m_body->CreateFixture(&fixtureDef));
    }
}

void Hill::generateMeshes() {
	int hillVerticesCount = 0;
    Point p;
    for (int i=0; i < MAX_TERRAIN_COORDINATES; i++) {
        p.x = m_terrainCoordinates[i].x;
		p.y = m_terrainCoordinates[i].y;
        m_snowVertices[hillVerticesCount] = Point(p.x, p.y - 320.0f);
        m_snowTexCoords[hillVerticesCount++] = Point(p.x/m_snowTexture->getPixelsWide(), 1.0f);
        m_snowVertices[hillVerticesCount] = Point(p.x, p.y + 10.0f);
        m_snowTexCoords[hillVerticesCount++] = Point(p.x/m_snowTexture->getPixelsWide(), 0);
    }
}

void Hill::draw(Renderer* renderer, const Mat4 &transform, uint32_t flags) {
    Node::draw(renderer, transform, flags);
    m_renderCmds[0].init(0.0f);
    m_renderCmds[0].func = CC_CALLBACK_0(Hill::onDraw, this, transform);
    renderer->addCommand(&m_renderCmds[0]);
}

    void Hill::onDraw(const Mat4 &transform) {
#if defined(NO_RENDERING)
		return;
#endif
		int start = abs((getPositionX()) / STEP_LENGTH) * 2;
		int numPoints = 1600 / STEP_LENGTH * 2;

		auto glProgram = getGLProgram();
		glProgram->use();
		glProgram->setUniformsForBuiltins(transform);

		GL::bindTexture2D(m_snowTexture->getName());
		GL::enableVertexAttribs(GL::VERTEX_ATTRIB_FLAG_POS_COLOR_TEX);

		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glEnableVertexAttribArray(GLProgram::VERTEX_ATTRIB_POSITION);
		glVertexAttribPointer(GLProgram::VERTEX_ATTRIB_POSITION, 2, GL_FLOAT, GL_FALSE, 0, m_snowVertices);
		glVertexAttribPointer(GLProgram::VERTEX_ATTRIB_TEX_COORD, 2, GL_FLOAT, GL_FALSE, 0, m_snowTexCoords);
		glDrawArrays(GL_TRIANGLE_STRIP, start, (GLsizei)numPoints);

}

Point Hill::getPosition(const int idx)
{
	Point pos;
	if (idx < MAX_TERRAIN_COORDINATES) {
		pos.x = m_terrainCoordinates[idx].x;
		pos.y = m_terrainCoordinates[idx].y;
	}
	return pos;
}