#ifndef _HILL_LAYER_H_
#define _HILL_LAYER_H_
 
#include "cocos2d.h"
 
USING_NS_CC;
 
#define STEP_LENGTH 40
#define MAX_TERRAIN_COORDINATES 1000

enum Type {
	Snow,
	Jump
};

struct Terrain {
	float x;
	float y;
	Type type;
};

enum ObstacleType {
	Rock1 = 0,
	Rock2,
	Lumikasa
};
 
class Hill: public Node {
    public:
        Hill();
        ~Hill();
         
        void initWorld(b2World* world);
        void destroy();
        void generateTerrain();
        void generateMeshes();
		Size generateObstacle(const Point pos);
		void createObstacleBody(Sprite *sprite, ObstacleType);
        void updatePhysics(int prevKeyIndex, int newKeyIndex);         
        void onDraw(const Mat4 &transform);
        virtual void draw(Renderer *renderer, const Mat4& transform, uint32_t flags);
        virtual void update(float dt);
		Point getPosition(const int idx);
		void addAvalanche(b2Body *avalanche);         
        virtual bool init();
    private:
		::Terrain m_terrainCoordinates[MAX_TERRAIN_COORDINATES];
         
        Point m_snowVertices[MAX_TERRAIN_COORDINATES*2];
        Point m_snowTexCoords[MAX_TERRAIN_COORDINATES*2];
         
        Texture2D* m_snowTexture;
        std::list<b2Fixture*> m_fixtures;
		std::list<b2Body *> m_obstacleBodies;
         
        b2Body* m_body;
        b2World* m_world;
		b2Body *m_avalanche;
		Size m_visibleSize;
		float m_maxPhysicsPoints;
         
        CustomCommand m_renderCmds[1];
public:		
        CREATE_FUNC(Hill);
    };
 
#endif