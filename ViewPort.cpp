#include <iostream>
#include "ViewPort.h"
#include "Definitions.h"
 
USING_NS_CC;
 
ViewPort::ViewPort(Node* node) : m_node(node), m_targetBody(nullptr) {
}
 
ViewPort::~ViewPort() {
     
}
 
Point ViewPort::getOffset() {
    float offsetX = 0;
    float offsetY = 0;
    float middleWidth = Director::getInstance()->getVisibleSize().width / 2.0f;
    float middleHeight = Director::getInstance()->getVisibleSize().height / 3.0f;
    offsetX = -(m_targetBody->GetPosition().x * SCALE_RATIO) + middleWidth;
    offsetY = -(m_targetBody->GetPosition().y * SCALE_RATIO) + middleHeight;
    return Point(offsetX, offsetY);
}
 
void ViewPort::setTarget(b2Body* targetBody) {
    m_targetBody = targetBody;
}
 
void ViewPort::update(float dt) {
	if (m_targetBody) {
		Point targetOffset = getOffset();
		m_node->setPosition(targetOffset);
	}
}