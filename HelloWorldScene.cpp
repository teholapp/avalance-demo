/****************************************************************************
 Copyright (c) 2017-2018 Xiamen Yaji Software Co., Ltd.
 
 http://www.cocos2d-x.org
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

#include "HelloWorldScene.h"
#include "SimpleAudioEngine.h"
#include "DebugLayer.h"
#include "Definitions.h"
#include "BodyEditorLoader.h"

USING_NS_CC;


Scene* HelloWorld::createScene()
{
	// 'scene' is an autorelease object
	auto scene = Scene::create();

	// 'layer' is an autorelease object
	auto layer = HelloWorld::create();

	// add layer as a child to scene
	scene->addChild(layer);

	// return the scene
	return scene;
}

// Print useful error message instead of segfaulting when files are not there.
static void problemLoading(const char* filename)
{
    printf("Error while loading: %s\n", filename);
    printf("Depending on how you compiled you might have to add 'Resources/' in front of filenames in HelloWorldScene.cpp\n");
}

void changeTextureToSprite(const char *filename, Sprite *sprite)
{
	float targetHeight = sprite->getContentSize().height;
	sprite->setTexture(filename);
	Size currentSize = sprite->getContentSize();
	float scale = targetHeight / currentSize.height;
	sprite->setScale(scale);
}


Sprite *HelloWorld::generateTexture(const char *sprite, float targetHeight)
{
	Sprite *spriteOrig = Sprite::create(sprite);
	Size contentSize = spriteOrig->getContentSize();
	float scale = (targetHeight / contentSize.height);
	spriteOrig->setFlippedY(true);
	spriteOrig->setScale(scale);
	Size targetSize(contentSize.width*scale, contentSize.height*scale);
	spriteOrig->setPosition(targetSize.width / 2, targetSize.height / 2);

	auto renderTexture = RenderTexture::create(targetSize.width, targetSize.height, Texture2D::PixelFormat::RGBA8888);
	renderTexture->begin();
	spriteOrig->visit();
	renderTexture->end();

	auto newSprite = Sprite::createWithTexture(renderTexture->getSprite()->getTexture());
	newSprite->setPosition(targetSize.width / 2, targetSize.height / 2);
	return newSprite;
}


// on "init" you need to initialize your instance
bool HelloWorld::init()
{
	if (!Layer::init())
	{
		return false;
	}

	m_gameOver = false;

	Size visibleSize = Director::getInstance()->getVisibleSize();
	Point origin = Director::getInstance()->getVisibleOrigin();

	//SET MOUSE LISTENER
	auto listener = EventListenerTouchOneByOne::create();
	listener->setSwallowTouches(true);

	listener->onTouchBegan = CC_CALLBACK_2(HelloWorld::onTouchBegan, this);
	listener->onTouchMoved = CC_CALLBACK_2(HelloWorld::onTouchMoved, this);
	listener->onTouchEnded = CC_CALLBACK_2(HelloWorld::onTouchEnded, this);

	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

	//Accelometer listener
	Device::setAccelerometerEnabled(true);
	// creating an accelerometer event
	auto listenerAcc = EventListenerAcceleration::create(CC_CALLBACK_2(
		HelloWorld::onAcceleration, this));

	_eventDispatcher->addEventListenerWithSceneGraphPriority(listenerAcc, this);

	// creating a keyboard event listener
	auto listenerKeyb = EventListenerKeyboard::create();
	listenerKeyb->onKeyPressed = CC_CALLBACK_2(HelloWorld::onKeyPressed, this);
	listenerKeyb->onKeyReleased = CC_CALLBACK_2(HelloWorld::onKeyReleased, this);

	_eventDispatcher->addEventListenerWithSceneGraphPriority(listenerKeyb, this);


	b2Vec2 gravity = b2Vec2(0.0f, -10.0f);
	m_world = new b2World(gravity);
	m_debugLayer = new DebugLayer(m_world);

	m_hillLayer = Hill::create();
	float scale;
#if !defined(NO_RENDERING)
	Sprite *sky = generateTexture("01 blue sky.png", visibleSize.height * 2);
	Sprite *clouds1 = generateTexture("03 clouds.png", visibleSize.height * 0.33);
	Sprite *clouds2 = generateTexture("03 clouds.png", visibleSize.height * 0.33);
	Sprite *snowTopMountains1 = generateTexture("04 snow top mountains.png", visibleSize.height * 0.5);
	Sprite *snowTopMountains2 = generateTexture("04 snow top mountains.png", visibleSize.height * 0.5);
	Sprite *mountains1 = generateTexture("05 midground mountains.png", visibleSize.height * 0.5);
	Sprite *mountains2 = generateTexture("05 midground mountains.png", visibleSize.height * 0.5);
	Sprite *trees1 = generateTexture("07 midground trees.png", visibleSize.height * 0.33);
	Sprite *trees2 = generateTexture("07 midground trees.png", visibleSize.height * 0.33);

	m_backGround = CCParallaxScrollNode::create();
	m_backGround->addInfiniteScrollXWithZ(1, Point(1, 1), Point(0, visibleSize.height / 1.5), clouds1, clouds2, NULL);
	m_backGround->addInfiniteScrollXWithZ(0, Point(1, 1), Point(0, visibleSize.height / 2), snowTopMountains1, snowTopMountains2, NULL);
	m_backGround->addInfiniteScrollXWithZ(3, Point(1, 1), Point(0, visibleSize.height / 4), mountains1, mountains2, NULL);
	m_backGround->addInfiniteScrollXWithZ(4, Point(1, 1), Point(0, visibleSize.height / 4), trees1, trees2, NULL);

	Sprite *fronTrees1 = generateTexture("08 front trees.png", visibleSize.height * 0.4);
	Sprite *fronTrees2 = generateTexture("08 front trees.png", visibleSize.height * 0.4);
	Sprite *fronTrees3 = generateTexture("08 front trees.png", visibleSize.height * 0.4);
	Sprite *fronTrees4 = generateTexture("08 front trees.png", visibleSize.height * 0.4);
	Sprite *fronTrees5 = generateTexture("08 front trees.png", visibleSize.height * 0.4);
	Sprite *fronTrees6 = generateTexture("08 front trees.png", visibleSize.height * 0.4);


	m_backGroundMid = CCParallaxScrollNode::create();
	m_backGroundMid->addInfiniteScrollXWithZ(4, Point(1, 1), Point(0, visibleSize.height / 6), fronTrees1, fronTrees2, fronTrees3, fronTrees4, fronTrees5, fronTrees6, NULL);
	this->addChild(sky, -2);
#endif
	m_hillLayer->initWorld(m_world);
	m_hillLayer->generateTerrain();
	m_hillLayer->generateMeshes();
	m_hillLayer->updatePhysics(0, 100);

	m_posX = m_hillLayer->getPosition(0).x + origin.x;
	m_posY = m_hillLayer->getPosition(0).y + origin.y;

	m_player = generateTexture("sprite.png", visibleSize.height / 4);
	m_hillLayer->addChild(m_player, 0);

	//Add avalance
	m_avalancheSprite = generateTexture("snowball.png", visibleSize.height / 2);
	float avalanchePos = m_posX + m_avalancheSprite->getContentSize().width;
	m_avalancheSprite->setPosition(CCPoint(avalanchePos, m_posY + m_avalancheSprite->getContentSize().height / 2));
	m_hillLayer->addChild(m_avalancheSprite, 0);

	m_player->setPosition(CCPoint(avalanchePos + m_player->getContentSize().width*4,
		m_posY + m_player->getContentSize().height / 2));

#if !defined(NO_RENDERING)
	this->addChild(m_backGround);
	this->addChild(m_backGroundMid);
#endif
	this->addChild(m_hillLayer);
	m_hillLayer->addChild(m_debugLayer);

	HelloWorld::definePlayer();
	defineAvalanche();
	m_camera = new ViewPort(m_hillLayer);
	m_camera->setTarget(m_playerBody);
	m_camera->update(0);


    auto closeItem = MenuItemImage::create(
                                           "CloseNormal.png",
                                           "CloseSelected.png",
                                           CC_CALLBACK_1(HelloWorld::menuCloseCallback, this));

    if (closeItem == nullptr ||
        closeItem->getContentSize().width <= 0 ||
        closeItem->getContentSize().height <= 0)
    {
        problemLoading("'CloseNormal.png' and 'CloseSelected.png'");
    }
    else
    {
        float x = origin.x + visibleSize.width - closeItem->getContentSize().width/2;
        float y = origin.y + closeItem->getContentSize().height/2;
        closeItem->setPosition(Vec2(x,y));
    }


    // create menu, it's an autorelease object
    auto menu = Menu::create(closeItem, NULL);
    menu->setPosition(Vec2::ZERO);
    this->addChild(menu, 1);

	m_gameRunning = false;
	setAccelerometerEnabled(true);

	m_distanceLabel = Label::createWithTTF("Distance: 0", "fonts/arial.ttf", 32);
	m_distanceLabel->setPosition(Vec2(visibleSize.width - 200, visibleSize.height - 20));
	m_distanceLabel->enableShadow();

	m_gameOverLabel = Label::createWithTTF("GAME OVER", "fonts/arial.ttf", 64);
	m_gameOverLabel->setPosition(Vec2(visibleSize.width / 2, visibleSize.height / 2));
	m_gameOverLabel->enableShadow();
	this->addChild(m_gameOverLabel);
	m_gameOverLabel->setVisible(false);

	this->addChild(m_distanceLabel);
    return true;
}

void HelloWorld::menuCloseCallback(Ref* pSender)
{
    //Close the cocos2d-x game scene and quit the application
    Director::getInstance()->end();
}

void HelloWorld::update(float dt) {
	int positionIterations = 10;
	int velocityIterations = 10;

	m_world->Step(dt, velocityIterations, positionIterations);
	
	static bool hit = false;
	if (m_playerBody) {
		for (b2ContactEdge* edge = m_playerBody->GetContactList(); edge; edge = edge->next) {
			if (edge->other == m_avalancheBody) {
				hit = true;
				changeTextureToSprite("hit.png", m_player);
			}
			else {
				if (m_playerBody->GetLinearVelocity().x < 0.0) {
					m_playerBody->GetFixtureList()[0].SetFriction(1.0);
				}
				else {
					m_playerBody->GetFixtureList()[0].SetFriction(0.002);
				}
			}
		}
	}

	static double distance = 0;

	if (hit) {
		if (m_playerBody) {
			m_camera->setTarget(nullptr);
			m_world->DestroyBody(m_playerBody);
			m_playerBody = nullptr;
		}
		else {
			if (m_gameOver == false && m_avalancheSprite->getPosition().x > m_player->getPosition().x) {
				m_player->setVisible(false);
				m_gameOver = true;
				m_gameOverLabel->setVisible(true);
				changeTextureToSprite("snowball_hit.png", m_avalancheSprite);
			}
		}
	}
	else {
		float distance = m_playerBody->GetPosition().x;
		char distanceStr[50];
		sprintf(distanceStr, "Distance: %.1f m", distance);
		m_distanceLabel->setString(&distanceStr[0]);
	}

	
	for (b2Body *body = m_world->GetBodyList(); body != NULL; body = body->GetNext())
		if (body->GetUserData())
		{
			CCSprite *sprite = (CCSprite *)body->GetUserData();
			if (!hit || sprite != m_player) {
				sprite->setPosition(ccp(body->GetPosition().x * SCALE_RATIO, body->GetPosition().y * SCALE_RATIO));
				sprite->setRotation(-1 * CC_RADIANS_TO_DEGREES(body->GetAngle()));
			}
		}

//	world->ClearForces();
	m_camera->update(dt);
#if !defined(NO_RENDERING)
	if (m_playerBody) {
		m_backGround->updateWithVelocity(Point((m_playerBody->GetLinearVelocity().x)*0.1*-1, 0), dt);
		m_backGroundMid->updateWithVelocity(Point((m_playerBody->GetLinearVelocity().x)*-1, 0), dt);
	}
#else
	world->DrawDebugData();
#endif
}

void HelloWorld::defineAvalanche()
{	
	b2FixtureDef avalancheFixture;
	b2CircleShape avalancheShape;
	b2BodyDef avalancheBodyDef;

	Size size = m_avalancheSprite->getContentSize()*m_avalancheSprite->getScale();
	avalancheFixture.density = 0.1;
	avalancheFixture.friction = 5.0;
	avalancheFixture.restitution = 0.0;
	avalancheShape.m_radius = size.width / 2 / SCALE_RATIO;
	avalancheFixture.shape = &avalancheShape;
	avalancheBodyDef.type = b2_dynamicBody;
	avalancheBodyDef.userData = m_avalancheSprite;
	avalancheBodyDef.position.Set(m_avalancheSprite->getPosition().x / SCALE_RATIO, m_avalancheSprite->getPosition().y / SCALE_RATIO);
	m_avalancheBody = m_world->CreateBody(&avalancheBodyDef);
	m_avalancheBody->SetAngularDamping(0.0f);
	m_avalancheBody->CreateFixture(&avalancheFixture);
	m_avalancheBody->ApplyTorque(m_avalancheBody->GetMass()*-2000, true);
	m_hillLayer->addAvalanche(m_avalancheBody);
}

void HelloWorld::definePlayer()
{
	b2PolygonShape playerShape;
	b2BodyDef playerBodyDef;

	ssize_t fileSize = 0;
	char* str = reinterpret_cast<char*>(CCFileUtils::sharedFileUtils()->getFileData("player7.json", "rb", &fileSize));
	std::string s(str);
	delete[] str;
	BodyEditorLoader bodyLoader(s);
	b2FixtureDef playerFixture;

	Size size = m_player->getContentSize()*m_player->getScale();
	playerFixture.density = 5;
	playerFixture.friction =  0.002;
	playerFixture.restitution = 0.2;
	playerBodyDef.type = b2_dynamicBody;
	playerBodyDef.userData = m_player;
	playerBodyDef.position.Set(m_player->getPosition().x / SCALE_RATIO, m_player->getPosition().y / SCALE_RATIO);
	m_playerBody = m_world->CreateBody(&playerBodyDef);
	m_playerBody->SetAngularDamping(50.0f);
	bodyLoader.attachFixture(m_playerBody, "Name", &playerFixture, 4.0f);
	float force = m_playerBody->GetMass() * 10;
	m_playerBody->SetGravityScale(4);
}

bool HelloWorld::onTouchBegan(Touch* touch, Event* event)
{
	return true;
}

void HelloWorld::onTouchMoved(Touch* touch, Event* event)
{
}

void HelloWorld::onTouchEnded(Touch* touch, Event* event)
{
	//Game starts
	if (m_gameRunning == false) {
		m_gameRunning = true;
		scheduleUpdate();
		float force = m_playerBody->GetMass() * 3;
		m_playerBody->ApplyLinearImpulse(b2Vec2(force, 0), m_playerBody->GetWorldCenter(), true);
	}

	if (!m_playerBody)
		return;

	float impulse = m_playerBody->GetMass() *  -30;
	float forward = m_playerBody->GetMass() *  0;
	m_playerBody->ApplyLinearImpulse(b2Vec2(forward, impulse), m_playerBody->GetWorldCenter(), true);
}

void HelloWorld::onAcceleration(Acceleration *acc, Event *unused_event)
{
	/*	if (!m_playerBody)
		return;

	if (acc->x < 0.15f && acc->x > -0.2f) {
        	m_playerBody->SetAngularDamping(10.0f);
	} else if (acc->x < -0.2f) {
		//Speedup
		float force = m_playerBody->GetMass() * -20;
		if (m_playerBody->GetLinearVelocity().x > 3)
			m_playerBody->ApplyLinearImpulse(b2Vec2(force, 0), m_playerBody->GetWorldCenter(), true);
	} else if (acc->x > 0.2f) {
		float force = m_playerBody->GetMass() * 75;
		m_playerBody->ApplyForce(b2Vec2(force, 0), m_playerBody->GetWorldCenter(), true);
	}
	*/
}


void HelloWorld::onKeyPressed(EventKeyboard::KeyCode keyCode, Event* event)
{

}

void HelloWorld::onKeyReleased(EventKeyboard::KeyCode keyCode, Event* event)
{
}