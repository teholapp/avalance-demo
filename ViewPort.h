#ifndef __ViewPort_H__
#define __ViewPort_H__
 
#include <Box2D/Box2D.h>
#include "cocos2d.h"
 
USING_NS_CC;
 
class ViewPort {

public:
    ViewPort(Node* node);
    ~ViewPort();
     
    void setTarget(b2Body* target);
    void update(float dt);
private:
    Point getOffset();
    Node* m_node;     
    b2Body* m_targetBody;
};
 
#endif